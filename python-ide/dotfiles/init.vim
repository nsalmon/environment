" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.local/share/nvim/plugged')

" Airline is a plugin that makes the status line look fancier.
" It requires a custom font (with arrows), and is completely optional
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" theme I use (if required, alternative : https://github.com/joshdick/onedark.vim)
Plug 'rakr/vim-one'

" Autocompletion for Python
Plug 'davidhalter/jedi-vim'

" Highlights new/mofified/deleted lines in the "gutter"
Plug 'airblade/vim-gitgutter'

" Fzf for ffffuzzzy search~
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

" Neomake for asynchronous linting and building
" Plug 'neomake/neomake'

" A beautiful autopep8. Have it bound to "ap"
Plug 'tell-k/vim-autopep8'

" Syntax checker for python
Plug 'nvie/vim-flake8'

" Initialize plugin system
call plug#end()

set updatetime=500

" Airline setup
set laststatus=2
let g:airline_powerline_fonts = 1
let g:airline_theme = "one"

" Turn on TrueColor
set termguicolors

" This colorscheme mimics a default Atom colorscheme which I quite like
set background=dark
colorscheme one

" Jedi-vim configuration
let g:jedi#show_call_signatures = 1 
let g:jedi#popup_select_first = 0
let g:jedi#completions_enabled = 1
autocmd FileType python setlocal completeopt-=preview

" Turn on line numbers
set nu
" Turn on syntax highlighting
syntax on
" It hides buffers instead of closing them.
" https://medium.com/usevim/vim-101-set-hidden-f78800142855
set hidden
" Highlights search results as you type vs after you press Enter
set incsearch
" Ignore case when searching
set ignorecase
set smartcase 
" Turns search highlighting on
set hlsearch
" Expands TABs into whitespaces
set expandtab
set shiftwidth=4  
" Exclude these files from *
set wildignore=*.swp,*.bak,*.pyc,*.class

if executable('ag')
  " Use ag over grep
  set grepprg=ag\ --nogroup\ --nocolor

  " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
  let g:ctrlp_user_command = 'ag %s -l --nocolor --ignore .git --ignore build-out --ignore build-opt --ignore build-dbg -g ""'

endif


" Ctrl+P opens a fuzzy filesearch window (powered by Fzf)
"nnoremap <C-p> :Files<CR>


" Switch to last active tab
let g:lasttab = 1
" I really like tt for switching between recent tabs
"nmap tt :exe "tabn ".g:lasttab<CR>
"au TabLeave * let g:lasttab = tabpagenr()

" A bit of autopep8 config
let g:autopep8_disable_show_diff=1
let g:autopep8_max_line_length=120
let g:flake8_max_line_length=120

" ap for a quick .py file formatting
nnoremap ap  :Autopep8<CR>

" This is a quick way to call search-and-replace on a current word
" nnoremap <Leader>s :%s/\<<C-r><C-w>\>//g<Left><Left>
" cc now hides those annoying search highlihghts after you're done searching
" nnoremap cc :let @/ = ""<cr>

" Flake8
nnoremap fl  :call Flake8()<CR>

" When yanking (copy with y), defaults to copying to system clipboard
set clipboard=unnamedplus

" type 'open' to open new file with fzf (fuzzy search) in a new tab
nnoremap open :call fzf#run({'right': winwidth('.') / 2, 'sink':  'vertical botright split' })<CR>

" when a Neovim terminal window is opened use standard VI <ESC> command to
" switch to normal mode
:tnoremap <Esc> <C-\><C-n>

" open terminal
nnoremap term :split term://zsh<CR>

" defaults to opening splits below rather than above
set splitbelow

" defaults to opening new vertical split to the right
set splitright

" launch pdb on current file
:noremap pdb :vsplit <CR>:term python -m ipdb %<CR>

" launch python on current file
:noremap python :vsplit <CR>:term python %<CR>
