#!/bin/sh

clear
export TERM="screen-256color"

# generate unique id, so that we can run multiple tmux
SESSION=$(uuidgen)
tmux new-session -d -s $SESSION

# SQL pane
# text editor
# psql interpreter
tmux new-window -t $SESSION:1 -n 'SQL'
tmux split-window -v
tmux resize-pane -D 7

# Python pane
# text editor
# bash to launch python script
# logs
tmux new-window -t $SESSION:2 -n 'PYTHON'
tmux select-window -t:$SESSION:2 'clear'
tmux split-window -v
tmux resize-pane -D 12
tmux select-pane -U
tmux split-window -h
tmux resize-pane -R 10


# misc pane
# e.g. bash to launch db restart, grafana restart, etc...
# text editor
tmux new-window -t $SESSION:3 -n 'MISC'
tmux split-window -v
tmux resize-pane -D 7

tmux attach-session -t $SESSION


