## Installation steps

Create following folder structure on host :
```
~/GIT
~/DATA
~/DOCKER_HOME
```

Run following command :
```
touch ~/DOCKER_HOME/.zsh_history
mkdir ~/DOCKER_HOME/.ipython
```
This will make sure we can sync up locally zsh shell history, and ipython shell history
 
Edit file : ~/DOCKER_HOME/.config  
with your email and name
```
[diff]
	tool = vimdiff
[merge]
	tool = vimdiff
[user]
	email = <YOUR EMAIL>
	name = <YOUR NAME>
```
 
Docker need to be installed on host.  
Run :
```
./build.sh
```


To have nice color settins with Pantheon terminal (Elementary OS) or Gnome terminal (Linux  mint) run :

```
./one-dark.sh
```
Source : https://github.com/Mayccoll/Gogh/themes/one-dark.sh
  
Then add the following in your .bashrc :
```
force_color_prompt=yes
```

## Launch

```
./start.sh python-ide
```

## Features

* python3
* install extra packages from requirements.txt
* ipython shell (from jupyter)
* X11 bindings to display plots (in start.sh script)
- vim
  * python3 support
  * autocompletion plugin => jedi
  * syntax highlighting
  * linters
  * autopep
  * solarized theme => use one theme instead (atom)
  * line number
  - [X] nerd tree ? => not useful, use FZF instead
* tmux
* zsh
  * solarized theme => one theme instead
  * powerline
- to run on GCE via docker
* be able to launch on another docker image
* terminal
  - nice fonts
  * one dark theme colorscheme
* sync up shell history with local disk (volume) + GIT / SSH creds
* python support for neovim
* fuzzy search to open file in vim => fzf (Ctrl-P or :FZF)
* python debugger
* diff tool => vim -d <FILE> <FILE> Ctrl-W to move between panes
   - train on how to use it effectively
- integration with jupyter-qtconsole => was not working, check later, but not sure is super useful
* jupyter notebook launch
* system clipboard

TODO :  
- switch between one dark / one light => change on both gnome/pantheon settings and VIM settings inside docker
 use VI terminals instead of tmux ? (OK but annoying to switch between tabs (need to be in normal mode then Ctrl+w)
- learn to use VIM macros
- add su possibility

## Inspired by :
https://hackernoon.com/clife-or-my-development-setup-67868b86cb57
https://blog.theodo.fr/2014/01/build-a-zen-command-line-environment-with-git-tmux-oh-my-zsh-mosh-and-docker/
https://github.com/danielmoi/alpine-neovim/blob/master/Dockerfile
https://www.cyfyifanchen.com/neovim-true-color/
https://www.integralist.co.uk/posts/dev-environments-within-docker-containers/
https://ibnusani.com/article/dockerizing-vim/
https://github.com/JAremko/alpine-vim/tree/master/alpine-vim-base
https://github.com/thesheff17/youtube/blob/master/vim/vimrc
https://realpython.com/vim-and-python-a-match-made-in-heaven/

## Useful commands

#### How to Copy / paste in VI / tmux

Copy in VI : y  
(init.vim modified to defaults to yanking to system clipboard)  
Paste in VI (from VI) : p  
Paste in VI or tmux / zsh (from system clipboard) : Ctrl+Shift+v  
Copy in tmux : Ctrl+B+[ then v (for selecting text) then y (to copy to system clipboard)  

#### VI : switch between files
:ls => list of open files  
:bp => go to previous file  
:bn => go to next file  
:bd => close current file  

#### VI : run linter (check that python code syntax is correct) - Use Flake8
fl

#### VI : autopep8 (reformat code automatically to python standards) - Use autopep8
ap

#### VI : diff 2 files
```
vi -d <FILE> <FILE>
```
Use Ctrl+w + Arrow keys, to move between files

#### VI : autocompletion
Ctrl+Space

#### VI : open terminal
term

#### VI : open new file with fuzzy search on new tab
open

#### VI : run python on current file
python

#### VI : run ipbd on current file (python debugger)
pdb

#### VI : tabs
new tab :   :tabe <FILE>  
go to next tab : gt  
go to previous tab : gT  

#### ipython : autocompletion
Tab

#### launch jupyter notebook
notebook

#### GIT : diff change before committing
gd <FILE>

=> this will open git default difftool (here vim) with side by side comparison / correction

#### VI : comment code
* select block (using v as usual)
* :norm i#

#### VI : uncomment code
* select block (using v as usual)
* :norm x

## VI cheatsheet

#### General

Nearly all commands can be preceded by a number for a repeat count. eg. 5dd delete 5 lines  
* \<Esc\> gets you out of any mode and back to command mode
* Commands preceded by : are executed on the command line at the bottom of the screen
* :help help with any command

#### Navigation

Cursor movement: ←h ↓j ↑k l→  
  
By words:  
w next word (by punctuation); W next word (by spaces)  
b back word (by punctuation); B back word (by spaces)  
e end word (by punctuation); E end word (by spaces)  
By line:  
0 start of line; ^ first non-whitespace  
$ end of line  
By paragraph:  
{ previous blank line; } next blank line  
By file:  
gg start of file; G end of file  
123G go to specific line number  
By marker:  
mx set mark x; 'x go to mark x  
'. go to position of last edit  
' ' go back to last point before jump  
Scrolling:  
^F forward full screen; ^B backward full screen  
^D down half screen; ^U up half screen  
^E scroll one line up; ^Y scroll one line down  
zz centre cursor line  

#### Editing

u undo; ^R redo  
. repeat last editing command  

#### Inserting

All insertion commands are terminated with <Esc> to return to command mode.

i insert text at cursor; I insert text at start of line  
a append text after cursor; A append text after end of line  
o open new line below; O open new line above  

#### Changing

All change commands except r and R are terminated with <Esc> to return to command mode.  
  
r replace single character; R replace multiple characters  
s change single character  
cw change word; C change to end of line; cc change whole line  
c<motion> changes text in the direction of the motion  
ci( change inside parentheses (see text object selection for more examples)  

#### Deleting

x delete char  
dw delete word; D delete to end of line; dd delete whole line  
d<motion> deletes in the direction of the motion  

#### Cut and paste

yy copy line into paste buffer; dd cut line into paste buffer  
p paste buffer below cursor line; P paste buffer above cursor line  
xp swap two characters (x to delete one character, then p to put it back after the cursor position)  
Blocks  
  
v visual block stream; V visual block line; ^V visual block column  
most motion commands extend the block to the new cursor position  
o moves the cursor to the other end of the block  
d or x cut block into paste buffer  
y copy block into paste buffer  
\> indent block
< unindent block  
gv reselect last visual block  

#### Global

:%s/foo/bar/g substitute all occurrences of "foo" to "bar"  
% is a range that indicates every line in the file  
/g is a flag that changes all occurrences on a line instead of just the first one  

#### Searching

/ search forward  
? search backward  
\* search forward for word under cursor; # search backward for word under cursor  
n next match in same direction; N next match in opposite direction  
fx forward to next character x; Fx backward to previous character x  
; move again to same character in same direction; , move again to same character in opposite direction  

#### Files

:w write file to disk  
:w name write file to disk as name  
ZZ write file to disk and quit  
:n edit a new file; :n! edit a new file without saving current changes  
:q quit editing a file; :q! quit editing without saving changes  
:e edit same file again (if changed outside vim)  
:e . directory explorer  

#### Windows

^Wn new window  
^Wj down to next window  
^Wk up to previous window  
^W\_ maximise current window  
^W= make all windows equal size  
^W+ increase window size  
^W- decrease window size  

#### Source Navigation

% jump to matching parenthesis/bracket/brace, or language block if language module loaded  
gd go to definition of local symbol under cursor; ^O return to previous position  
^] jump to definition of global symbol (requires tags file); ^T return to previous position (arbitrary stack of positions maintained)  
^N (in insert mode) automatic word completion  

## More on VIM

https://moolenaar.net/habits.html
http://www.viemu.com/a-why-vi-vim.html
