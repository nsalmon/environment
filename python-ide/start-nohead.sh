#!/bin/bash

if [ -z "$1" ]
  then
    echo "No argument supplied - Need to provide docker image name you want to use"
fi

DOCKER_IMAGE=$1
CONTAINER_NAME=$1

docker rm $CONTAINER_NAME

#--user 1000:1000 \
docker run -it --network host \
        --name $CONTAINER_NAME \
        --env NB_UID=1000 \
        --env NB_GID=1000 \
        -v $HOME/DATA:/DATA \
        -v $HOME/GIT:/GIT \
        -v /var/run/docker.sock:/var/run/docker.sock \
        -v $HOME/DOCKER_HOME/.zsh_history:/home/nicolas/.zsh_history \
        -v $HOME/DOCKER_HOME/.gitconfig:/home/nicolas/.gitconfig \
        -v $HOME/DOCKER_HOME/.ipython:/home/nicolas/.ipython \
        -v $HOME/.ssh:/home/nicolas/.ssh \
        $DOCKER_IMAGE

# change xhost so that we can display Matplotlib plot out of the docker image
export containerId=$(docker ps -l -q -f name=$CONTAINER_NAME)
xhost +local:`docker inspect --format='{{ .Config.Hostname }}' $containerId`
