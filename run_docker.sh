#!/bin/sh

if [ $1 = "notebook" ]
   then
     LOCAL_DATA_PATH="/home/$USER/"
     NOTEBOOK_DIR="/home/$USER/GIT/"
     docker run -it -e NB_UID=1000 -e NB_GID=100 -v $LOCAL_DATA_PATH:/DATA -v $NOTEBOOK_DIR:/home/jovyan/work -p 8888:8888 jupyter/scipy-notebook
#  then docker run -v /home/nicolas/DATA/:/DATA -v /home/nicolas/GIT/:/GIT -it -p 8888:8888 --net=host python-rl-dl /opt/conda/bin/jupyter notebook --allow-root
fi

if [ $1 = "run" ]
  then docker run -v /home/nicolas/DATA/:/DATA -v /home/nicolas/GIT/:/GIT -it -p 8888:8888 --net=host python-rl-dl python $2
fi

if [ $1 = "debug" ]
  then docker run -v /home/nicolas/DATA/:/DATA -v /home/nicolas/GIT/:/GIT -it -p 8888:8888 --net=host python-rl-dl python -m ipdb $2
fi

# not to run as root
# -u `ud -u $USER`
if [ $1 = "console" ]
  then docker run -v /home/nicolas/DATA/:/DATA -v /home/nicolas/GIT/:/GIT -it -p 8888:8888 --net=host python-rl-dl /bin/bash
fi

if [ $1 = "console_root" ]
  then docker run -u `id -u $USER` -v /home/nicolas/DATA/:/DATA -v /home/nicolas/GIT/:/GIT -it -p 8888:8888 --net=host python-rl-dl /bin/bash
fi

if [ $1 = "tensorboard" ]
  then docker run -u `id -u $USER` -v /home/nicolas/DATA/:/DATA -v /home/nicolas/GIT/:/GIT -it -p 6006:6006 --net=host python-rl-dl tensorboard --logdir=/DATA/TF
fi

if [ $1 = "hugo" ]
  then docker run -v /home/nicolas/DATA/:/DATA -v /home/nicolas/GIT/:/GIT -it -p 8888:8888 --net=host felicianotech/docker-hugo /bin/bash
fi
