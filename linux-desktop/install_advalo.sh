#!/bin/sh

./install_unix_base.sh
./optional/install_atom.sh
./optional/install_chrome.sh
./optional/install_java.sh
./optional/install_intellij.sh
./optional/install_docker.sh

# TODO
# - install VPN
# - private/public key gen for GIT / AWS
# - install AWS
