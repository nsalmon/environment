#!/bin/sh

echo "###### Installing UNIX tools"

apt update
apt upgrade -y

apt-get install software-properties-common
add-apt-repository ppa:neovim-ppa/stable
apt update -y && apt install -y htop tmux neovim git zsh uuid-runtime xclip

rm /usr/bin/vi && ln -s /usr/bin/nvim /usr/bin/vi
