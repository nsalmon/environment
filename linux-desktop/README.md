
#### Install
##### IPV6 issues
Need to do following :
```vi /etc/gai.conf
```
uncomment (line 54) :
```precedence ::ffff:0:0/96  100
```

Might also be needed to edit /etc/sysctl.conf :
```
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
net.ipv6.conf.lo.disable_ipv6 = 1
```
And then run :
```
sudo sysctl -p
```
#### Install Elememtary OS Loki

If screen freeze after login
sudo apt-get remove pantheon
sudo apt-get install pantheon

More permanent solution:
mv /etc/xdg/autostart/at-spi-dbus-bus.desktop /etc/xdg/autostart/at-spi-dbus-bus.disabled
