#!/bin/sh

echo "###### Installing Zsh"
sudo apt-get install zsh
sudo apt-get install git-core
wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh
chsh -s `which zsh`
sudo apt-get install fonts-powerline

# + TODO : edit ~/.zshrc
# ZSH_THEME="agnoster"
