#!/bin/sh

echo "###### Installing Pycharms"

# PyCharms install
add-apt-repository ppa:ubuntu-desktop/ubuntu-make
apt-get update
apt-get install ubuntu-make
umake ide pycharm
