#!/bin/sh

wget https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-183.0.0-linux-x86_64.tar.gz -O /tmp/google-cloud-sdk-183.0.0-linux-x86_64.tar.gz
#tar -xzvf /tmp/google-cloud-sdk-183.0.0-linux-x86_64.tar.gz
#/tmp/./google-cloud-sdk/install.sh

# Below was not working with elementary OS (Loki)

# Create an environment variable for the correct distribution
#export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"

# Add the Cloud SDK distribution URI as a package source
#echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list

# Import the Google Cloud Platform public key
#curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

# Update the package list and install the Cloud SDK
#sudo apt-get update && sudo apt-get install google-cloud-sdk
