#!/bin/sh

echo "###### Installing Dot files"

# Install dot files
DOTFILES=`pwd`
echo $DOTFILES

ln -s $DOTFILES/../../tmux/tmux.conf ~/.tmux.conf
ln -s $DOTFILES/../../tmux/setup_tmux.sh ~/setup_tmux.sh

echo "
alias tm='sh ~/setup_tmux.sh'
alias solarized='bash ~/GIT/environment/solarized.sh'
" >> ~/.bashrc
