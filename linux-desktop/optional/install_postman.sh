#!/bin/sh

echo "###### Installing PostMan"

wget https://dl.pstmn.io/download/latest/linux64 -O /tmp/postman.tar.gz
sudo tar -xzf /tmp/postman.tar.gz -C /opt
rm /tmp/postman.tar.gz
ln -s /opt/Postman/Postman /usr/bin/postman
