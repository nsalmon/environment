#!/bin/sh

echo "###### Installing Docker"

# necessary to use command : apt-add-repository
apt-get install software-properties-common python-software-properties
apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'
apt-get update
apt-cache policy docker-engine
apt-get install -y docker-engine
apt-get install -y docker-compose
# make sure $USER can run docker without sudo
sudo usermod -aG docker $USER
gpasswd -a $USER docker
newgrp docker
