#!/bin/sh

wget https://update.gitter.im/linux64/gitter_4.1.0_amd64.deb -O /tmp/gitter.deb
dpkg -i /tmp/gitter.deb
apt-get install -f
rm /tmp/gitter.deb
