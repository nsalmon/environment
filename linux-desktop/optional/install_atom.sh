#!/bin/sh

echo "###### Installing Atom"

add-apt-repository ppa:webupd8team/atom
apt update
apt install -y atom
