#!/bin/sh

echo "###### Installing Gephi"

wget https://github.com/gephi/gephi/releases/download/v0.9.2/gephi-0.9.2-linux.tar.gz  -O /tmp/gephi.tar.gz
tar -xzf /tmp/gephi.tar.gz -C /opt
rm /tmp/gephi.tar.gz
ln -s /opt/gephi-0.9.2/bin/gephi /usr/bin/gephi
