#!/bin/sh

echo "###### Installing GIT LFS support"

# Git setup
# Git LFS support
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | os=ubuntu dist=trusty bash
# note : might need to
apt-get install git-lfs
git lfs install
git config --global user.name "Nicolas SALMON"
git config --global user.email "nicolas.salmon@gmail.com"
# avoid issue with timeout for credentials
git config credential.helper 'cache --timeout=3600'
