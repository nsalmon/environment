#!/bin/sh

echo "###### Installing IntelliJ"

INTELLIJ_VERSION="2017.2.4"

wget https://download.jetbrains.com/idea/ideaIC-$INTELLIJ_VERSION-no-jdk.tar.gz -O /tmp/intellij.tar.gz
mkdir -p /opt/intellij
tar -xf /tmp/intellij.tar.gz --strip-components=1 -C /opt/intellij
rm /tmp/intellij.tar.gz
ln -s /opt/intellij/bin/idea.sh /usr/bin/idea

# TODO : have settings saved
