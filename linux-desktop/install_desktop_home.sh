#!/bin/sh

./install_unix_base.sh
#optional/install_atom
./optional/install_chrome.sh
#optional/install_java
#optional/install_intellij
./optional/install_docker.sh
./optional/install_git_fls.sh
#optional/install_pycharms.sh
./optional/install_dot_file.sh

# remove transparent background in elementary OS Loki
# gsettings set org.pantheon.terminal.settings background "rgba(37, 46, 50, 1.0)"
