#!/bin/sh

install_unix_base
optional/install_atom
optional/install_chrome
optional/install_java
optional/install_intellij
optional/install_docker
optional/install_gcloud
optional/install_git_fls
optional/install_home_apps
#optional/install_pycharms
optional/install_dot_files

# remove transparent background in elementary OS Loki
gsettings set org.pantheon.terminal.settings background "rgba(37, 46, 50, 1.0)"
