#!/bin/sh

# generate unique id, so that we can run multiple tmux
SESSION=$(uuidgen)
tmux new-session -d -s $SESSION

tmux new-window -t $SESSION:0 -n 'main'
tmux split-window -h
tmux split-window -v
tmux split-window -v
#tmux selection-pane -t 1
#tmux send-keys "ssh ..." C-m

#tmux new-window 'mutt'
tmux attach-session -t $SESSION


